const { check, validationResult } = require('express-validator/check');

function errorHandling(err,res){
  console.log(err)
  if (err.name==="MongoError"){
    switch (err.code){
      case 11000:
        res.status(422).send({
          status:"Error",
          message:"Duplicate key",
          details:err.message
        })
        break;

      default:
        res.status(422).send({
          status:"New Error!!!!!!",
          message:"Come capture this eror",
          error_code:err.code,
          details:err.message
        })
    }
  }

  else if (err.name="ValidatorError"){
    console.log(err)
    res.status(422).send({
      status:"Error",
      message:"Missing field",
      details:err.message
    })
  }
}

function validateInput(req,res){
  var errors = validationResult(req);
  if (!errors.isEmpty()) {
    var errorMessage=errors.mapped()
    var details=[]
    Object.keys(errorMessage).map(function(key, index) {
      var value = errorMessage[key].msg;
      details.push(value)
    });
    res.status(422).json({
      status:"Error",
      message:"Missing field",
      details
    });
    return true
  }
  return false
}

function queryHandler(result,success,fail){
  result?success():fail()
}

function errorHandler(err,req,res,next){
  if (err){
    return errorHandling(err,res)
  }
  next()
}
module.exports={
  errorHandling,
  queryHandler,
  validateInput,
  errorHandler
}
