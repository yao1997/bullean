const mongoose=require('mongoose');
const assert = require('assert')
const ip = require('ip');

function connectDB(){
  var connectionURL='mongodb://localhost:27017/bullean';
  if (ip.address()=="192.168.56.1"){
    connectionURL="mongodb://localhost:27017/admin"
  }
  // DB conenction and mongoose setup
  //mongoose.connect('mongodb://root:Yao199789!@localhost/admin',{useNewUrlParser: true},err=>{
  mongoose.connect(connectionURL,{useNewUrlParser: true},err=>{
    assert.equal(null, err);
  })

  var db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function() {
    console.log("Connected to DB")
  });
}

module.exports={
  connectDB
}
