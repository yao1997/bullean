const ratio=require("./ratioCalculation.js")

function calculateLiquidityIndex(response){
  var currentRatio=ratio.getCurrentRatio(response)
  var quickRatio=ratio.getQuickRatio(response)
  var score=(currentRatio.score+quickRatio.score)/2
  return {
    currentRatio,
    quickRatio,
    score
  }
}

function calculateLeverageIndex(response){
  var debtRatio=ratio.getDebtRatio(response)
  var debtToEquityRatio=ratio.getDebtToEquityRatio(response)
  return {
    debtRatio,
    debtToEquityRatio,
    score:(debtRatio.score+debtToEquityRatio.score)/2
  }
}

function calculateProfitabilityIndex(response){
  var profitMargin=ratio.getProfitMargin(response)
  var roa=ratio.getROA(response)
  return {
    profitMargin,
    roa,
    score:(profitMargin.score+roa.score)/2
  }
}

function calculateEfficiencyIndex(response){

}

function calculateMarketValueIndex(response){

}

module.exports={
  calculateLiquidityIndex,
  calculateLeverageIndex,
  calculateProfitabilityIndex,
  calculateEfficiencyIndex,
  calculateMarketValueIndex
}
