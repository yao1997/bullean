//const iex_url = "https://api.iextrading.com/1.0";
const iex_url = 'https://sandbox.iexapis.com/v1'
const token='Tpk_f359bc401a5446f4b6876cffd821e22c'
const request = require('request');
const Stock=require("../model/Stock.js");
const rp = require('request-promise');
const index=require("./indicesCalculation.js")
const ratio=require("./ratioCalculation.js")

function get(path,callBack){
  if (path.includes('?'))
      request.get({url:iex_url+path+'&token='+token,json:true},callBack)
  request.get({url:iex_url+path+'?token='+token,json:true},callBack)
}

function getPromise(path){
  if (path.includes('?'))
      return rp.get(iex_url+path+'&token='+token)
  return rp.get(iex_url+path+'?token='+token)
}
//To load all data from iex into databse during initialisation
function initializeStocks(res){
  get("/ref-data/symbols",function(err,response){
    Stock.insertMany(response.body).then(function(doc){
      res.send({
        status:"success",
        message:"Stock Collection is initialized successfully"
      })
    }).catch(err=>{
      console.log(err)
    })
  })
}

//Initialize with company profile and logo
function initializeStockWithCompanyProfile(stock){
  return getPromise("/stock/"+stock.symbol+"/company").then(function(response){
    response=JSON.parse(response)
    stock.industry=response.industry
    stock.CEO=response.CEO
    stock.website=response.website
    stock.tags=response.tags
    stock.description=response.description
    stock.save()
    return stock;
  })
}

function getEarnings(stock){
  return getPromise("/stock/"+stock.symbol+"/earnings").then(function(response){
    return response=JSON.parse(response)
  })
}

function getFinancials(stock){
  console.log("Get Financials called")
  return getPromise("/stock/"+stock.symbol+"/financials?period=annual").then(function(response){
    return response=JSON.parse(response)
  })
}

function getKeyStats(stock){
  console.log("Get Key Stats called")
  return getPromise("/stock/"+stock.symbol+"/stats").then(function(response){
    return response=JSON.parse(response)
  }).catch(function(err){
  })
}

function getQuote(stock){
  console.log("Get Quote called")
  return getPromise("/stock/"+stock.symbol+"/quote").then(function(response){
    return response=JSON.parse(response)
  })
}

function analyze(stock){
  return getFinancials(stock).then(response=>{
    var stockData=stock.toObject();
    var analyzedStock={
      ...stockData,
      liquidityIndex:index.calculateLiquidityIndex(response),
      financialLeverageIndex:index.calculateLeverageIndex(response),
      profitabilityIndex:index.calculateProfitabilityIndex(response)
    }
    return analyzedStock
  }).then(analyzedStock=>{
    return getKeyStats(analyzedStock).then(response=>{
      analyzedStock={
        ...analyzedStock,
        marketValueIndex:{pbRatio:ratio.getPB(response)}
      }
      return analyzedStock
    })
  }).then(analyzedStock=>{
    return getQuote(analyzedStock).then(response=>{
      var pbRatio=analyzedStock.marketValueIndex.pbRatio
      var peRatio=ratio.getPE(response)
      analyzedStock={
        ...analyzedStock,
        marketValueIndex:{
          pbRatio,
          peRatio,
          score:(pbRatio.score+peRatio.score)/2
        }
      }
      return analyzedStock
    })
  })
}

//Operating cash flow ratio= cashflow/totalLiabilities
module.exports={
  initializeStocks,
  analyze,
  initializeStockWithCompanyProfile
}
