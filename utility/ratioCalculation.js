/******
Liquidity ratios
  i.  Current ratio                   (done) (weak)
  ii. Quick ratio                     (done) (weak)
  iii. Operating cash flow Ratio [6]
Financial leverage ratios
  i.  Debt ratio                      (done) (weak)
  ii. Debt-to equity ratio [7]				(done) (weak)
Profitability ratios
  i.  Profit margin                   (well done)
  ii. Return On Assets (ROA)          (well done)
  iii.Return On Equity (ROE) [10]     (Total assets - Total liabilities)
Efficiency Ratios
  i.  Accounts Receivable turnover
  ii. Fixed Asset Turnover
  iii.Sales to Inventory
  iv. Sales to Net Working Capital		(done) (weak)
  v.  Accounts Payable to Sales
  vi. Stock Turnover Ratio [9]
	vii. Total Asset Turnover Ratio 		(done) (weak)
Market Value Ratios
  i.  Price to Book Ratio (PB)        (done)
  ii. Price to Cash Flow Ratio
  iii.Price to Earning Ratios (PE)    (done)
  iv. Price to Sales Ratio
  v.  Earning per Share (EPS)         (done)
  vi. Dividend Yield [8]              (TODO)

*******/

/**********************************************/
// Goof if >2, Bad if <1
function getCurrentRatio(result){
	result=result.financials[0];
	//var currentLiabilities=result.shareholderEquity-result.currentAssets
	var currentRatio=result.currentAssets/result.currentDebt;
	var score
	switch (true){
		case currentRatio > 2:
			score=5;break;
		case currentRatio < 1:
			score=0;break;
		default:
			score=(currentRatio-1)*5
	}
	return {
		score:+score.toFixed(4),
		data:currentRatio===Infinity?2.5700:currentRatio,
		name:"Current ratio"
	};
}

// 1 > quickratio > 2
function getQuickRatio(result){
	result=result.financials[0];
	var quickRatio=result.currentCash/result.currentDebt;
	var score;
	switch (true){
		case quickRatio > 2:
			score=5;break;
		case quickRatio < 1:
			score=0;break;
		default:
			score=(quickRatio-1)*5
	}
	return {
		score:+score.toFixed(4),
		data:quickRatio===Infinity?2.5700:quickRatio,
		name:"Quick ratio"
	};
}

/**********************************************/





/**********************************************/
// 0.4 < debtRatio < 0.8
function getDebtRatio(result){
	result=result.financials[0];
	var totalLiabilities=result.totalAssets-result.shareholderEquity
	var debtRatio=totalLiabilities/result.totalAssets;
	var score;
	switch (true){
		case debtRatio > 0.8:
			score=0;break;
		case debtRatio < 0.4:
			score=5;break;
		default:
			score=(5-(debtRatio-0.4)*12.5);
	}
	return {
		score:+score.toFixed(4),
		data:debtRatio,
		name:"Debt ratio"
	};
}

// 1 < debtToEquityRatio <2
function getDebtToEquityRatio(result){
	result=result.financials[0];
	var totalLiabilities=result.totalAssets-result.shareholderEquity
	var debtToEquityRatio=totalLiabilities/result.shareholderEquity
	var score;
	switch (true){
		case debtToEquityRatio > 2:
			score=0;break;
		case debtToEquityRatio < 1:
			score=5;break;
		default:
			score=5-(debtToEquityRatio-1)*5;
	}
	return {
		score:+score.toFixed(4),
		data:debtToEquityRatio,
		name:"Debt-to-Equity ratio"
	};
}

/**********************************************/





/**********************************************/
// 3%<profit margin<10%
function getProfitMargin(result){
	result=result.financials[0];
	var profitMargin=result.netIncome/result.operatingRevenue*100;
	switch (true){
		case profitMargin > 10:
			score=5;break;
		case profitMargin < 0:
			score=0;break;
		default:
			score=profitMargin/2;
	}
	return {
		score:+score.toFixed(4),
		data:profitMargin,
		name:"Profit Margin(%)"
	};
}

// 5%<roa<20%
function getROA(result){
	result=result.financials[0];
	var roa= result.netIncome/result.totalAssets
	switch (true){
		case roa > 0.2:
			score=5;break;
		case roa < 0.00:
			score=0;break;
		default:
			score=roa*25
	}
	return {
		score:+score.toFixed(4),
		data:roa,
		name:"Return On Assets"
	};
}

/***********************************************/


/**********************************************/
//  <0 ; 0-13 ; 14-20 ; 21-28 ; 28+
function getPE(result){
	peRatio=result.peRatio;
	var score;
	switch (true){
		case peRatio<10:
			score=5;break;
		case peRatio>30 || peRatio<0:
			score=0;break;
		default:
			score=5-(peRatio/6)
	}
	return {
		data:peRatio,
		score:+score.toFixed(4),
		name:"Price to Earning Ratio"
	}
}

// 1 < PB < 15
function getPB(result){
	var pbScore;
	pbRatio=result.priceToBook;
	switch (true){
		case pbRatio <1:
			score=5;break;
		case pbRatio >=15:
			score=0;break;
		default:
			score=(5-(pbRatio/3))
	}
	returnPb={
		score:+score.toFixed(4),
		data:pbRatio,
		name:"Price to Book ratio"
	}
	return (returnPb);
}
/**********************************************/


/*********************************************
// 0.5<tatr<1
function getTotalAssetTurnoverRatio(result){
	result=result.financials[0];
	return totalAssets/totalRevenue;
}

function getSalesToWorkingCapitalRatio(ratio){
	result=result.financials[0];
	return result.operatingIncome/(result.currentAssets/result.currentDebt)
}

function getEPS(result){
	result=result.earnings;
	var eps=result[0].actualEPS+result[1].actualEPS+result[2].actualEPS+result[3].actualEPS;
	console.log("The EPS is :"+eps);
}



function getEpsGrowth(result){
	var quarter1=result.earnings[3].actualEPS;
	var quarter2=result.earnings[2].actualEPS;
	var quarter3=result.earnings[1].actualEPS;
	var quarter4=result.earnings[0].actualEPS;

	var increment1=(quarter3-quarter4)/quarter4;
	var increment2=(quarter2-quarter3)/quarter3;
	var increment3=(quarter1-quarter2)/quarter2;

	var epsGrowth=(increment1+increment2+increment3)/3*100
	var epsGrowthScore;

	if (epsGrowth>20){
		epsGrowthScore=5
	}
	else if(epsGrowth<0){
		epsGrowthScore=0
	}
	else{
		epsGrowthScore=epsGrowth/4
	}
	if (epsGrowth==Infinity){
		epsGrowth=0;
	}
	var returnEpsGrowth={
		data:epsGrowth,
		score:epsGrowthScore,
		name:"EPS growth rate"
	}
	return returnEpsGrowth
}
// PB is smaller better
function getPB(result){
	var pbScore;
	pbRatio=result.priceToBook;
	if (pbRatio<1){
		pbScore=5
	}
	else if (pbRatio>=15){
		pbScore=0
	}
	else{
		pbScore=(5-(pbRatio/3))
	}
	returnPb={
		score:pbScore.toFixed(2),
		data:pbRatio,
		name:"P/B ratio"
	}
	return (returnPb);
}

function getNetProfitGrowth(result){
	thisQuarter=result.financials[0].netIncome;
	lastQuarter=result.financials[1].netIncome;
	netProfitGrowth=(thisQuarter-lastQuarter)/lastQuarter;
	var netProfitGrowthScore;
	if (netProfitGrowth>100){
		netProfitGrowthScore=5;
	}
	else if (netProfitGrowth<0){
		netProfitGrowthScore=0;
	}
	else{
		netProfitGrowthScore=netProfitGrowth/20;
	}
	netProfitGrowthScore=netProfitGrowthScore.toFixed(2)
	returnNetProfitGrowth={
		score:netProfitGrowthScore,
		data:netProfitGrowth,
		name:"Net Profit Growth Rate"
	}
	return (returnNetProfitGrowth)
}

function getCashFlowCoverageRatio(result){
	result=result.financials[0]
	var cashFlowCoverageRatioScore;
	var cashFlowCoverageRatio=result.cashFlow/result.totalDebt
	if (result.totalDebt==null){
		cashFlowCoverageRatioScore=5
	}
	else{
		if (cashFlowCoverageRatio>1){
			cashFlowCoverageRatioScore=5
		}
		else if (cashFlowCoverageRatio<0){
			cashFlowCoverageRatioScore=0
		}
		else{
			cashFlowCoverageRatioScore=cashFlowCoverageRatio
		}
	}
	var reuturnCashFlowCoverageRatio={
		score:cashFlowCoverageRatioScore,
		data:cashFlowCoverageRatio,
		name:"Cash Flow Coverage Ratio"
	}
	return reuturnCashFlowCoverageRatio
}

function getShortRatio(result){
	var shortInterestRatio=result.shortRatio;
	var shortInterestRatioScore;
	if (shortInterestRatio>5){
		shortInterestRatioScore=0
	}
	else if (shortInterestRatio<0){
		shortInterestRatioScore=5
	}
	else {
		shortInterestRatioScore=5-(shortInterestRatio-1)
	}
	var returnShortInterestRatio={
		data:shortInterestRatio,
		score:shortInterestRatioScore,
		name:"Short Interest Ratio"
	}
	return returnShortInterestRatio;
}

function getEPS(result){
	result=result.earnings;
	var eps=result[0].actualEPS+result[1].actualEPS+result[2].actualEPS+result[3].actualEPS;
	console.log("The EPS is :"+eps);
}

//TODO: complete it
function getDividendYield(result){

}
/**********************************************/




module.exports={
	getDebtRatio,
	getDebtToEquityRatio,
	getProfitMargin,
	getCurrentRatio,
	getROA,
	getQuickRatio,
	getPB,
	getPE
}
