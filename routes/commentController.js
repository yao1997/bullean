const express = require('express');
const router=express.Router();
const Comment = require("../model/Comment.js")
const User=require("../model/Comment.js")
const jwt = require('../utility/jwtUtility.js');
const utility= require("../utility/utility.js");
const { body, validationResult,header } = require('express-validator/check');
const auth =require("./authenticationController").middleware

router.post("/createComment",[
  auth.checkToken,
  body("comment","comment cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("stockId","Stock id cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  var data=req.body
  var comment =new Comment({...data,authorId:jwt.getUserId(req.headers.token)})
  comment.save(function(err){
    err?utility.errorHandling(err,res):
    res.send({
      status:"Success",
      message:"Comment posted",
      data:comment
    })
  })
})

router.post("/replyComment",[
  auth.checkToken,
  body("comment","comment cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("stockId","Stock id cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("mainComment","Main comment cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  // success
  var comment= new Comment({...req.body,authorId:jwt.getUserId(req.headers.token)})
  return comment.save((err)=>{
    return res.send({
      status:"Success",
      message:"Comment posted",
      data:comment
    })
  })
})

router.delete("/deleteComment",[
  auth.checkToken,
  body("commentId","Comment id cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  var data=req.body
  var userId=req.cookies.userId
  var comment = Comment.findById({_id:data.commentId}).then((comment)=>{
    //Check if comment is found
    if (!comment){
      return res.send({
        status:"Error",
        message:"Comment does not exist",
        details:"The comment is not in the table"
      })
    }
    //Check if user is author
    if (comment.authorId!=jwt.getUserId(req.get("token"))){
      return res.status(422).send({
        status:"Error",
        message:"User is not author",
        details:"User is not the author who posted the comment."
      })
    }
    //delete comment
    return comment.deleteComment((result)=>{
      res.send({
        status:"Success",
        message:"Comment deleted",
        data:comment
      })
    })
  })
})

router.put("/editComment/:commentId",[
  body("comment","comment cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  var comment = Comment.findById(req.params.commentId).then(comment=>{
    if (!comment){
      return res.send({
        status:"Error",
        message:"Comment does not exist",
        details:"The comment is not in the table"
      })
    }
    //Check if user is author
    var userId=req.cookies.userId
    if (comment.authorId!=jwt.getUserId(req.get("token"))){
      return res.status(422).send({
        status:"Error",
        message:"User is not author",
        details:"User is not the author who posted the comment."
      })
    }
    //Update comment
    return Comment.findByIdAndUpdate(req.params.commentId,{$set:req.body},{new:true}).then(result=>{
      res.send({
        status:"Success",
        message:"Comment updated",
        data:result
      })
    })
  })
})

router.get("/getStockComment/:stockId",[

],function(req,res,next){
  var stockId=req.params.stockId;
  Comment.find({stockId}).populate("replies").populate('authorId').exec((err,comments)=>{
    Comment.populate(comments,{
      path:"replies.authorId",
      model:"User"
    },function(err,comments){
      return res.send({
        status:"Success",
        message:"Comment found",
        data:comments
      })
    })
  })
})
module.exports=router
