const express = require('express');
const router=express.Router();
const iex=require("../utility/iexUtility.js")
const Stock = require("../model/Stock.js")

router.get("/initializeStocks",function(req,res){
  iex.initializeStocks(res)
})

router.get("/",function(req,res){
  var stocks=Stock.find({},function(err,stocks){
    res.send({stocks})
  })
})

router.get("/:symbol",function(req,res){
  var query=req.params.symbol.toUpperCase();
  var stock=Stock.findOne({symbol:query},function(err,stock){
    //Stock does not exist
    if (stock===null){
       return res.send({
        status:"error",
        message:"No stock found",
        data:"The symbol does not exist"
      })
    }
    //Stock has already been initialized
    if (stock.industry!==""){
      return iex.analyze(stock).then(analysedStock=>{
        return res.send({
          status:"success",
          message:"Record found and analyzed",
          data:analysedStock
        })
      }).catch(err=>{
        return res.send({
          status:"error",
          message:"IEX does not provide this stock",
          data:"Please search for another stock. :)"
        })
      })
    }
    //Stock has not been initialized yet
    iex.initializeStockWithCompanyProfile(stock).then(updatedStock=>{
      return iex.analyze(updatedStock)
    }).then(analysedStock=>{
      return res.send({
        status:"success",
        message:"Record found,updated and analyzed.",
        data:analysedStock
      })
    }).catch(err=>{
      return res.send({
        status:"error",
        message:"IEX does not provide this stock",
        data:"Please search for another stock. :)"
      })
    })
  })
})

router.get("/initialize/:symbol",function(req,res){
  var query=req.params.symbol.toUpperCase();
  var stock=Stock.findOne({symbol:query},function(err,stock){
    iex.initializeStockWithCompanyProfile(stock).then(function(response){
      response=JSON.parse(response)
      stock.industry=response.industry
      stock.CEO=response.CEO
      stock.website=response.website
      stock.tags=response.tags
      stock.description=response.description
      stock.save(function(err){
        err?console.log(err):
        res.send({
          status:"success",
          message:"Stock found and updated.",
          data:stock
        })
      })
    })
  })
})

router.get("/searchByName/:name",function(req,res){
  var stock=Stock.find({name:{ $regex: '.*'+req.params.name+'.*', $options: 'i' }}).then(stock=>{
    //console.log(stock)
    if (stock.length==0){
      return res.send({
        status:"error",
        message:"No stock found.",
        data:"Sorry, we cant find any stock that match the query, please try again :("
      })
    }
    return res.send({
      status:"success",
      message:"Stock found.",
      data:stock
    })
  })
})

router.get("/searchBySymbol/:symbol",function(req,res){
  var stock=Stock.find({symbol:{ $regex: '.*'+req.params.symbol.toUpperCase()+'.*', $options: 'i' }}).then(stock=>{
    if (stock.length==0){
      return res.send({
        status:"error",
        message:"No stock found.",
        details:"Sorry, we cant find any stock that match the query, please try again :("
      })
    }
    return res.send({
      status:"success",
      message:"Stock found.",
      data:stock
    })
  })
})

router.delete("/deleteStockBySymbol/:symbol",[

],function(req,res){
  var query=req.params.symbol.toUpperCase();
  var stock=Stock.findOne({symbol:query},function(err,stock){
    if (!stock){
      return res.send({
        status:"error",
        message:"No stock found.",
        details:"Sorry, we cant find any stock that match the query, please try again :("
      })
    }
    Stock.deleteOne({_id:stock.id},function(err){
      return res.send({
        status:"success",
        message:"Stock Deleted.",
        data:null
      })
    })
  })
})
module.exports=router;
