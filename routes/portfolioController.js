const express = require('express');
const router=express.Router();
const Portfolio = require("../model/Portfolio.js")
const Stock=require("../model/Stock.js")
const User=require("../model/User.js")
const utility= require("../utility/utility.js");
const jwt = require('../utility/jwtUtility.js');
const auth =require("./authenticationController").middleware

const { body, validationResult,header } = require('express-validator/check');

router.get("/getPortfolio",[
  auth.checkToken
],function(req,res,next){
  var portfolioId=jwt.verifyToken(req.get("token")).portfolio
  Portfolio.findOne({_id:portfolioId}).populate({path:"stocks",model:"Stock"}).then(portfolio=>{
    return res.send({
      status:"Success",
      message:"Portfolio found",
      details:portfolio
    })
  })
})

router.put("/addStock",[
  auth.checkToken,
  body("stockSymbol","Stock Symbol cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  var symbol=req.body.stockSymbol.toUpperCase();
  var portfolioId=jwt.verifyToken(req.get("token")).portfolio
  Stock.findOne({symbol}).then(stock=>{
    if (!stock){
      return res.send({
        status:"Error",
        message:"Stock does not exist",
        details:"The stock symbol is not in the database"
      })
    }
    Portfolio.findById(portfolioId).then(portfolio=>{
      if (portfolio.stocks.indexOf(stock.id)!==-1){
        return Portfolio.populate(portfolio,{path:'stocks',model:"Stock"},function(){
          res.status(422).send({
            status:"Error",
            message:"Stock is already in portfolio",
            details:portfolio
          })
        })
      }
      portfolio.stocks.push(stock.id)
      portfolio.save(function(){
        return Portfolio.populate(portfolio,{path:'stocks'},function(){
           res.send({
            status:"Success",
            message:"Stock is added into portfolio",
            details:portfolio
          })
        })
      })
    })
  })
})

router.put("/removeStock",[
  body("stockSymbol","Stock Symbol cannot be empty").not().isEmpty({ignore_whitespace:true}),
  auth.checkToken
],function(req,res,next){
  if (utility.validateInput(req,res)){
    return ;
  }
  var symbol=req.body.stockSymbol.toUpperCase();
  var portfolioId=jwt.verifyToken(req.get("token")).portfolio
  Stock.findOne({symbol}).then(stock=>{
    if (!stock){
      return res.send({
        status:"Error",
        message:"Stock does not exist",
        details:"The stock symbol is not in the database"
      })
    }
    Portfolio.findById(portfolioId).then(portfolio=>{
      if (portfolio.stocks.indexOf(stock.id)===-1){
        return Portfolio.populate(portfolio,{path:'stocks',model:"Stock"},function(){
          res.send({
            status:"Error",
            message:"Stock is not in portfolio",
            details:"The stock is not in the portfolio and hence cannot be removed."
          })
        })
      }
      portfolio.stocks.splice(portfolio.stocks.indexOf(stock.id),1)
      portfolio.save(()=>{
        return Portfolio.populate(portfolio,{path:'stocks',model:"Stock"},function(){
          res.send({
            status:"Success",
            message:"Stock is removed from portfolio",
            details:portfolio
          })
        })
      })
    })
  })
})


module.exports=router;
