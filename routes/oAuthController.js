const mongoose=require('mongoose');
const express = require('express');
const router=express.Router();
const {google} = require('googleapis');
const User=mongoose.model("User");
const jwt = require('../utility/jwtUtility.js');
const dotenv = require('dotenv')

dotenv.config()
console.log(process.env.REDIRECT_URL)
const oauth2Client = new google.auth.OAuth2(
  "974746454042-ekagtqsd073t33id4a4gsunqteci5qe0.apps.googleusercontent.com",
  "fTwAucrxPrVWL5MREfSqxhSk",
  process.env.REDIRECT_URL
);

const scopes=[
  "profile",
  "email",
  'https://www.googleapis.com/auth/plus.login'
]

router.post("/authentication/oAuth",function(req,res){
  var data=req.body
  oauth2Client.getToken(data.code).then(response=>{
    oauth2Client.setCredentials(response.tokens)

    var oauth2=google.oauth2({
      auth:oauth2Client,
      version:'v2'
    })
    return oauth2.userinfo.get(function(err,response){
      User.findOne({email:response.data.email}).populate('portfolio').exec(function(err,user){
        var data
        if (!user){
          return (registerAndAuthenticateUser(response.data,res))
        }
        data= authenticateUser(user)
        return res.status(200).send({
          status:"success",
          message:"OAuth success",
          data
        })
      })
    })
  }).catch(err=>{
    console.log(err.response)
  })
})

function registerAndAuthenticateUser(data,res){
  var user=new User({
    email:data.email,
    firstName:data.family_name,
    lastName:data.given_name
  })
  return user.save().then(user=>{
    res.send({
      status:"success",
      message:"OAuth and register success",
      data:authenticateUser(user)
    })
  })
}

function authenticateUser(user){
  var result=user.toObject();
  var token=jwt.generateToken(result)
  return ({...result,token})
}
/*oauth2Client.on('tokens', (tokens) => {
  if (tokens.refresh_token) {
    // store the refresh_token in my database!
    console.log(tokens.refresh_token);
  }
  console.log(tokens.access_token);
});*/

module.exports=router
