const mongoose=require('mongoose');
const express = require('express');
const router=express.Router();
const jwt = require('../utility/jwtUtility.js');
const utility= require("../utility/utility.js")
const UserModel=require('../model/User');
const User=mongoose.model("User");
const { body, validationResult,header } = require('express-validator/check');
const jwtErrorHandling = require('../utility/jwtUtility.js').errorHandling;
const bcrypt=require("bcryptjs");


var middleware={
  checkEmail:function(req,res,next){
    var data=req.body;
    User.findOne({email:data.email}).then(user=>{
      return user?res.status(422).send({
        status:"Error",
        message:"Email Registered",
        details:"The email has aleady been used"
      }):next()
    })
  },
  checkToken:function(req,res,next){
    var token=req.headers.token
    if (!token){
      return res.status(401).send({
        status:"Error",
        message:"Unauthenticated",
        details:"Please log in first."
      })
    }
    jwt.verifyToken(token,function(err,decoded){
      var result;
      if (err){
        result=jwtErrorHandling(err.name)
        return res.json(result)
      }
      else if (req.cookies.userEmail!==decoded.email || req.cookies.userId!==decoded.id || req.cookies.token!==decoded.tokein || req.cookies.portfolio!==decoded.portfolio){
        //jwt.setCookies(res,decoded)
      }
      next()
    })
  }
}

router.post("/authentication/register",[
  body("email",{
    exists:"Please insert email",
    isEmail:"Invalid Email"
  }).exists({checkNull:true}).isEmail(),
  body("password","Password cannot be empty and must be at least 6 characters long").not().isEmpty({ignore_whitespace:true}).isLength({ min: 6, max:12 }),
  body("firstName","First name cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("lastName","Last name cannot be empty").not().isEmpty({ignore_whitespace:true}),
  middleware.checkEmail
],function(req,res){
  if (utility.validateInput(req,res)){
    return ;
  }
  var data=req.body;

  var user=new User({
    email:data.email,
    firstName:data.firstName,
    lastName:data.lastName,
    password:data.password,
  })

  user.save(function(err){
    err?utility.errorHandling(err,res):
    res.status(200).send({
      status:"success",
      message:"User is registered successfully"
    })
  })
})

router.post("/authentication/login",[
  body("email",{
    exists:"Please insert email",
    isEmail:"Invalid Email"
  }).exists({checkNull:true}).isEmail(),
  body("password","Password cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res){
  if (utility.validateInput(req,res)){
    return ;
  }

  var data=req.body;
  User.findOne({email:data.email}).populate('portfolio').exec(function(err,user){
    if (!user){
      return res.status(401).send({
        status:"error",
        message:"User does not exist",
        details:"The email is not registered"
      })
    }
    if (!user.password){
      return res.status(401).send({
        status:"error",
        message:"Fail",
        details:"Gmail user does not use password to log in. Please log in with google"
      })
    }
    if (!bcrypt.compareSync(data.password, user.password)){
      return res.status(401).send({
        status:"error",
        message:"Fail",
        details:"The password is incorrect"
      })
    }
    User.populate(user,{
      path:"portfolio.stocks",
      model:"Stock"
    },function(err,user){
      user.authentication(data,user,res)
    })
  })
})


router.post("/authentication/verifyToken",[
  header("token","Please provide token").not().isEmpty({ignore_whitespace:true})
],function(req,res){
  var data=jwt.verifyToken(req.headers.token)
  res.send({
    status:"success",
    message:"Token verification success",
    data
  })
})

router.put("/authentication/changePassword",[
  middleware.checkToken,
  body("password","Old Password cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("newPassword","New Password cannot be empty and must be at least 6 characters long").not().isEmpty({ignore_whitespace:true}).isLength({ min: 6, max:12 }),
  body("confirmPassword","Confirm password cannot be empty").not().isEmpty({ignore_whitespace:true})
],function(req,res){
  var data=req.body
  if (utility.validateInput(req,res)){
    return ;
  }
  if (data.newPassword!==data.confirmPassword){
    return res.status(422).send({
      status:"Error",
      message:"New password is different confirm password",
      details:"Please reinsert password"
    })
  }
  User.findById(jwt.getUserId(req.headers.token)).then(user=>{
    if (!user.password){
      return res.status(422).send({
        status:"Error",
        message:"Google Account",
        details:"Google Account user does not have password."
      })
    }
    if (!bcrypt.compareSync(data.password, user.password)){
      return res.status(422).send({
        status:"Error",
        message:"Wrong password",
        details:"Inserted password is wrong."
      })
    }
    var salt=bcrypt.genSaltSync(10)
    console.log(data.newPassword)
    user.password=data.newPassword
    return user.save(()=>res.send({
      status:"success",
      message:"User password is changed successfully",
      data:user
    }))
  })
})

module.exports={router,middleware}
