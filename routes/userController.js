const express = require('express');
const router=express.Router();
const User=require("../model/User.js");
const jwt=require("../utility/jwtUtility.js")
const { body, check,header,oneOf } = require('express-validator/check');
const utility=require("../utility/utility.js")

router.get("/getProfile",function(req,res){
  var token=req.headers.token;
  var data=jwt.verifyToken(token)
  if (data){
    return User.findOne({_id:data.id}).then(user=>{
      res.status(200).send({
        status:"success",
        message:"User profile is retrieved successfully.",
        data:user
      })
    })
  }
  return res.status(422).send({
    status:"Error",
    message:"Cannot verify token.",
    details:"Please login again."
  })
})

router.put("/updateProfile",[
  body("firstName","First name cannot be empty").not().isEmpty({ignore_whitespace:true}),
  body("lastName","Last name cannot be empty").not().isEmpty({ignore_whitespace:true}),
],function(req,res){
  if (utility.validateInput(req,res)){
    return ;
  }
  var token=req.headers.token;
  var userId=jwt.verifyToken(token).id
  var data=req.body
  if (!userId){
    return res.status(422).send({
      status:"Error",
      message:"Cannot verify token.",
      details:"Please login again."
    })
  }
  User.findOneAndUpdate({_id:userId},{...data,updatedAt:new Date()},{new:true}).then(user=>{
    return res.status(200).send({
      status:"success",
      message:"User profile is updated successfully.",
      data:user
    })
  })
})

router.delete("/deleteUser",[],(req,res,next)=>{
  var userId=req.cookies.userId
  User.deleteOne({_id:userId},function(err){
    if (err){
      return res.status(422).send({
        status:"Error",
        message:"something went wrong",
        data:err
      })
    }
    res.clearCookie("userEmail");res.clearCookie("token");res.clearCookie("userId");
    res.clearCookie("portfolio");res.clearCookie("userLastName");res.clearCookie("userFirstName")
    return res.status(200).send({
      status:"success",
      message:"User is deleted.",
      data:null
    })
  })
})


module.exports=router
