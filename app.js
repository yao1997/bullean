const express = require("express");
const createError =require("http-errors");
const cookieParser = require('cookie-parser')
const app = express();
const mongoose=require('mongoose');
const authenticationRouter=require('./routes/authenticationController.js');
const oAuthController=require("./routes/oAuthController.js");
const stockRouter=require("./routes/stockController.js");
const commentRouter=require("./routes/commentController.js");
const userRouter=require("./routes/userController.js");
const portfolioRouter=require("./routes/portfolioController.js");
const jwt = require('./utility/jwtUtility.js');
const db=require("./utility/dbUtility.js")
const cors = require('cors')
const bodyParser = require('body-parser');
const utility = require('./utility/utility.js');

const corsOptions = {
  origin:["http://edward97.com", "http://localhost:8080"] ,
  optionsSuccessStatus: 200,
  credentials:true
}

db.connectDB();


app.use(cors(corsOptions))
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use(cookieParser())
app.listen(3000);


app.use("/",authenticationRouter.router)
app.use("/",oAuthController)
app.use("/user",userRouter)
app.use("/stock",stockRouter)
app.use("/comment",commentRouter)
app.use("/portfolio",portfolioRouter)
//app.use("/",utility.errorHandler)

app.use(function(err,req,res,next){
  err?console.log(err):next()
})

app.use(function(req, res, next) {
  next(createError(404));
});


/*
*/
