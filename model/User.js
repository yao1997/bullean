const mongoose=require('mongoose');
const schemas=require("./Schemas.js");
const userSchema=schemas.userSchema;
const bcrypt=require("bcryptjs");
const utility= require("../utility/utility.js")
const jwt= require("../utility/jwtUtility.js")
const Portfolio=require("./Portfolio.js")

//console.log(Portfolio)
userSchema.methods.registerUser=function(data){
  User.create({
    email:data.email,
    firstName:data.firstName,
    lastName:data.lastName,
    password:data.password
  },function(err){
    if(err){
      console.log(err+"")
    }
  })
}

userSchema.pre("save",function(){
  var user=this;
  if (user.isModified('password') && user.password){
    var salt=bcrypt.genSaltSync(10)
    user.password=bcrypt.hashSync(user.password,salt);
    var portfolio=new Portfolio({userId:user.id})
    portfolio.save()
    user.portfolio=portfolio.id
  }
  else{
    var portfolio=new Portfolio({userId:user.id})
    portfolio.save()
    user.portfolio=portfolio.id
  }
});


userSchema.methods.authentication=(data,user,res)=>{
  try{
    if (bcrypt.compareSync(data.password, user.password)){
      var result=user.toObject();
      var token=jwt.generateToken(result);
      jwt.setCookies(res,{...result,token});
      res.send({
        status:"success",
        message:"Authentication success",
        data:{...result,token}
      })
    }
    else{
      res.status(422).send({
        status:"Error",
        message:"Authentication Failed",
        details:"The password is invalid"
      })
    }
  }catch(err){
    console.log(err)
  }
}

//This link a model to the migration
 var User = mongoose.model('User', userSchema,"User");
 module.exports=User
