const mongoose=require('mongoose');
const assert = require('assert')
const Schema = mongoose.Schema;

/*************************
Schemas start here
*************************/

/************** User Schema **************/
var userSchema = new Schema({
  email:{type:String,unique:true,required:true},
  firstName:{type:String,required:true},
  lastName:{type:String,required:true},
  password:{type:String},
  createdAt:{type:Date,default:Date.now},
  updatedAt:{type:Date,default:null},
  portfolio:{type:Schema.Types.ObjectId,ref:"Portfolio"},
},{collection:"User"});
userSchema.virtual("fullName").get(function(){
  return this.firstName +" "+this.lastName;
}).
set(function(fullName){
  this.firstName=fullName.substr(0,fullName.indexOf(" "))
  this.lastName=fullName.substr(fullName.indexOf(" ")+1,)
})

userSchema.set('toObject', {
  getters: true,
  transform:(dot,ret,options)=>{
    delete ret._id
    //delete ret.id
    delete ret.password
    delete ret.__v
    return ret
  }
});
userSchema.set('toJSON', {
    virtuals: true
});
userSchema.set("validateBeforeSave",true);

/*****************************************/


/************* Stock Schema **************/
var stockSchema = new Schema({
  symbol:{type:String,unique:true,required:true},
  name:{type:String},
  createdAt:{type:Date,default:Date.now},
  isEnabled:{type:Boolean,default:true},
  type:{type:String,required:true,default:"cs"},
  iexId:{type:String,required:true},
  industry:{type:String,default:""},
  website:{type:String,default:""},
  tags:{type:[String],default:[]},
  CEO:{type:String,default:""},
  description:{type:String,default:""},
  logo:{type:String,default:""}
},{collection:"Stock"});

stockSchema.set('toObject', {
  getters: true,
  transform:(dot,ret,options)=>{
    delete ret.__v
    return ret
  }
});

/*****************************************/


/************* Comment Schema **************/
var commentSchema = new Schema({
  authorId:{type:Schema.Types.ObjectId,ref:"User",required:true},
  comment:{type:String,required:true,required:true},
  replies:{type:[Schema.Types.ObjectId],ref:"Comment"},
  stockId:{type:Schema.Types.ObjectId,ref:"Stock",required:true},
  createdAt:{type:Date,default:Date.now},
  mainComment:{type:Schema.Types.ObjectId,ref:"Comment"}
},{collection:"Comment"});

commentSchema.set('toObject', {
  getters: true,
  transform:(dot,ret,options)=>{
    console.log(ret)
    delete ret._id
    //delete ret.password
    delete ret.__v
    return ret
  }
});
commentSchema.set('toJSON', {
    virtuals: true
});
/*****************************************/

/************* Portfolio Schema **************/
var portfolioSchema = new Schema({
  userId:{type:Schema.Types.ObjectId,ref:"User",required:true},
  stocks:{type:[Schema.Types.ObjectId],ref:"Stock",default:[]},
  createdAt:{type:Date,default:Date.now}
},{collection:"Portfolio"});

portfolioSchema.set('toObject', {
  getters: true,
  transform:(dot,ret,options)=>{
    //delete ret.password
    delete ret.__v
    return ret
  }
});

/*****************************************/


module.exports={
  stockSchema,
  userSchema,
  commentSchema,
  portfolioSchema
}
