
# Bullean (Back-end)

Bullean is a web app that helps all the rookie in performing stock fundamental analysis and add them into portfolio. After so, user can check their portfolio performance and decide if they want to edit it or keep the portfolio.

Bullean get all of the stock data from IEX Trading api (https://iextrading.com/developer/docs/), after which bullean will perform a series of calculation to analyze stocks.

This project is about Bullean backend, which is built with mongodb, nodejs and express and deployed on a ubuntu virtual private server.


## Before we start

There are a few things that you might want to check out :
1. Youtube Video : https://www.youtube.com/watch?v=20BZrX6u264&feature=youtu.be
2. API Documentation : https://documenter.getpostman.com/view/3161448/RWguvGJR
3. Bitbucket Repository : https://bitbucket.org/yao1997/bullean/src/master/
4. Postman Collections and Environment : Check zip file ;)

## End-point Checklist

There are 4 models right now in Bullean app which are User, Stock, Portfolio and Comment. Below are the end points for the api:

1. **POST** Authentication - Register
2. **POST** Authentication - Login
3. **PUT** Authentication - Update Password
4. **GET** User - Get Profile
5. **PUT** User - Update Profile
6. **DELETE** User -  Delete User
7. **GET**  Stock - Get Stock List
8. **GET** Stock - Get Stock
9. **GET** Stock -  Search By Name
10. **DELETE**  Stcok - Delete By Symbol
11. **POST**  Comment - Create Comment
12. **POST**  Comment - Reply Comment
13. **PUT** Comment - Update Comment
14. **DELETE** Comment -  Delete Comment
15. **GET** Portfolio - Get Portfolio
16. **PUT** Portfolio - Add Stock
17. **PUT** Portfolio - Remove Stock    


## Things I have done:
- [x] Deployed : On Ubuntu Server
- [x] 5 GET, 4 PUT, 4 POST, 3 DELETE
- [x] 4 Models and Schema which are related
- [x] Repo Usage : with consistent commits
- [x] Input Validation
- [x] Authentication : Bcrypt and JWT Token
- [x] Third Party API request
- [x] Usage of Tools in Dev experience (please check tool used and dependencies)
- [x] Well organized code and file structure
- [x] Researches and calculation on stock ratios and indices.

## Tools Used
1. Atom Text Editor (https://atom.io/)
2. Windows cli
3. Postman (https://www.getpostman.com/)
4. MongoDB Compass (https://www.mongodb.com/products/compass)
5. Putty (https://www.putty.org/)
6. MCedit (http://www.mcedit.net/)
7. Filezilla (https://filezilla-project.org/)
8. Sourcetree (https://www.sourcetreeapp.com/)
9. Robo 3T (https://robomongo.org/)

## Dependencies

1.  Nodemon (https://github.com/remy/nodemon)
2.  Cookie-Parser (https://www.npmjs.com/package/cookie-parser)
3.  Http-errors (https://www.npmjs.com/package/http-errors)
4.  Bcryptjs (https://www.npmjs.com/package/bcryptjs)
5.  Express-validator (https://github.com/express-validator/express-validator)
6.  jsonWebToken (https://www.npmjs.com/package/jsonwebtoken)
7.  request (https://www.npmjs.com/package/request)
8.  request-promise (https://github.com/request/request-promise)
9.  express (https://expressjs.com/)
10. mongoose (https://mongoosejs.com/)
11. mongodb (https://www.mongodb.com/)


## Installation
If you are willing to clone the project. Kindly pull the repository.
```
git clone https://yao1997@bitbucket.org/yao1997/bullean.git```
```

Then install the dependencies (Make sure that you have nodejs and npm installed properly)
```
npm install
```
And the project can be run via the command
```
npm start
```

## TODO
1.  Fix the bug where when user is created, 2 portfolio might be created.(supposedly only 1 should)
2.  Add the function to remove deleted comment from Main comment "replies" field
3.  Add Portfolio risk and return analysis (Research required)
4.  Add "News" Model and related functions.


## Server Configuration

The project is deployed on an ubuntu server, on port 81 because port 80 is currently in use for an apache web server

- nginx v1.14
- npm v6.4.1
- mongodb v4.0.3
- nodejs v8.12.0
